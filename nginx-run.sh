#!/usr/bin/env bash

sed -i -e "s/worker_processes 5/worker_processes $(cat /proc/cpuinfo | grep processor | wc -l)/" /etc/nginx/nginx.conf
exec nginx